# vpc.tf | VPC Configuration

data "aws_vpc" "aws-vpc"{
  filter {
    name = "tag:Name"
    values = ["Gitlab-*"]
  }
}

data "aws_subnet_ids" "pub_sub"{
  vpc_id = data.aws_vpc.aws-vpc.id
  tags = {
    Name = "gitlab-artif-shared-pub*"
  }
}
/*
resource "aws_vpc" "aws-vpc" {
  cidr_block           = "10.10.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support   = true
  tags = {
    Name        = "${var.app_name}-vpc"
    Environment = var.app_environment
  }
}
*/

