# alb.tf | Load Balancer Configuration

resource "aws_alb" "application_load_balancer" {
  count             = var.lb_enabled ? 1 : 0
  #count              = "1"
  name               = "${var.app_name}-${var.app_environment}-alb"
  internal           = false
  load_balancer_type = "application"
  subnets            = element(data.aws_subnet_ids.pub_sub.*.ids, count.index)
  security_groups    = [aws_security_group.load_balancer_security_group.id]

  tags = {
    Name        = "${var.app_name}-alb"
    Environment = var.app_environment
  }
}

resource "aws_security_group" "load_balancer_security_group" {
  vpc_id = data.aws_vpc.aws-vpc.id
  name   = "harness-testing-alb-sg"

  ingress {
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  tags = {
    Name        = "${var.app_name}-sg"
    Environment = var.app_environment
  }
}

resource "aws_lb_target_group" "target_group" {
  name        = "${var.app_name}-${var.app_environment}-tg"
  port        = 80
  protocol    = "HTTP"
  target_type = "ip"
  vpc_id      = data.aws_vpc.aws-vpc.id

  health_check {
    healthy_threshold   = "3"
    interval            = "300"
    protocol            = "HTTP"
    matcher             = "200"
    timeout             = "3"
    path                = "/v1/healthcheck"
    unhealthy_threshold = "2"
  }

  tags = {
    Name        = "${var.app_name}-lb-tg"
    Environment = var.app_environment
  }
}

resource "aws_lb_target_group" "target_group_1" {
  name        = "${var.app_name}-${var.app_environment}-tg-1"
  port        = 80
  protocol    = "HTTP"
  target_type = "ip"
  vpc_id      = data.aws_vpc.aws-vpc.id

  health_check {
    healthy_threshold   = "3"
    interval            = "300"
    protocol            = "HTTP"
    matcher             = "200"
    timeout             = "3"
    path                = "/v1/healthcheck"
    unhealthy_threshold = "2"
  }

  tags = {
    Name        = "${var.app_name}-lb-tg-1"
    Environment = var.app_environment
  }
}

resource "aws_lb_listener" "listener" {
  count             = var.lb_enabled ? 1 : 0
  #count = "1"
  load_balancer_arn = aws_alb.application_load_balancer[count.index].id
  port              = "80"
  protocol          = "HTTP"

   default_action {
     type             = "forward"
     target_group_arn = aws_lb_target_group.target_group.id
   }

  #default_action {
  #  type = "redirect"

   # redirect {
   #   port        = "443"
    #  protocol    = "HTTPS"
     # status_code = "HTTP_301"
    #}
  #}
}

resource "aws_lb_listener" "listener-http-80" {
  count             = var.lb_enabled ? 1 : 0
  #count = "1"
  load_balancer_arn = aws_alb.application_load_balancer[count.index].id
  port              = "8081"
  protocol          = "HTTP"

  #ssl_policy      = "ELBSecurityPolicy-2016-08"
  #certificate_arn = "<certificate-arn>"

  default_action {
    target_group_arn = aws_lb_target_group.target_group_1.id
    type             = "forward"
  }
}

data "aws_route53_zone" "zone" {
  name = var.route53_hosted_zone_name
 # zone = var.zone
}

resource "aws_route53_record" "terraform" {
  count             = var.lb_enabled ? 1 : 0
  #count = "1"
  zone_id = data.aws_route53_zone.zone.zone_id
  name    = "${var.app_name}-${var.app_environment}-${var.aws_region}"
  type    = "A"
  #ttl     = var.ttl

  alias {
    name                   = aws_alb.application_load_balancer[count.index].dns_name
    zone_id                = aws_alb.application_load_balancer[count.index].zone_id
    evaluate_target_health = true
  }
}

